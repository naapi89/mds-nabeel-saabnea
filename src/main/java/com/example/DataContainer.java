package com.example;

public class DataContainer {

	private String key;
	private Object data;
	private DoubleLinkedNode node;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public DoubleLinkedNode getNode() {
		return node;
	}

	public void setNode(DoubleLinkedNode node) {
		this.node = node;
	}

	@Override
	public String toString() {
		return "DataContainer [key=" + key + ", data=" + data + ", node=" + node + "]";
	}

}
