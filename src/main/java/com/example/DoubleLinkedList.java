package com.example;

public class DoubleLinkedList {

	private DoubleLinkedNode head = null;;
	private DoubleLinkedNode tail = null;;

	public DataContainer add(String key, Object value) {
		DoubleLinkedNode dln = new DoubleLinkedNode();
		DataContainer dc = new DataContainer();
		dc.setKey(key);
		dc.setData(value);
		dc.setNode(dln);

		dln.setData(dc);
		dln.setNext(tail);
		dln.setPrevious(null);

		this.tail = dln;
		if (this.head == null) {
			this.head = dln;
		}

		return dc;
	}

	public void remove(DataContainer dataContainer) {
		DoubleLinkedNode dln = dataContainer.getNode();

		if (dln == null) {
			return;
		}

		if (dln.getPrevious() != null && dln.getNext() != null) {
			dln.getPrevious().setNext(dln.getNext());
			dln.getNext().setPrevious(dln.getPrevious());
		} else if (dln == head) {
			this.head = this.head.getPrevious();
		} else if (dln == tail) {
			this.tail = this.tail.getNext();
		}
	}

	public DataContainer removeHead() {
		DataContainer removed = null;

		if (this.head != null) {
			removed = this.head.getData();
			this.head = this.head.getPrevious();
		}

		return removed;
	}

	public DoubleLinkedNode getHead() {
		return head;
	}

	public void setHead(DoubleLinkedNode head) {
		this.head = head;
	}

	public DoubleLinkedNode getTail() {
		return tail;
	}

	public void setTail(DoubleLinkedNode tail) {
		this.tail = tail;
	}

}
