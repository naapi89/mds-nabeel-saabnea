package com.example;

public class DoubleLinkedNode {

	private DataContainer data = null;

	private DoubleLinkedNode previous = null;
	private DoubleLinkedNode next = null;

	public DataContainer getData() {
		return data;
	}

	public void setData(DataContainer data) {
		this.data = data;
	}

	public DoubleLinkedNode getPrevious() {
		return previous;
	}

	public void setPrevious(DoubleLinkedNode previous) {
		this.previous = previous;
	}

	public DoubleLinkedNode getNext() {
		return next;
	}

	public void setNext(DoubleLinkedNode next) {
		this.next = next;
	}

}
