package com.example;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

public class MyDataStructure {

	private HashMap<String, DataContainer> hashmap;

	private int maxCapacity;

	private DoubleLinkedList queue;

	public MyDataStructure(int capacity) {
		this.maxCapacity = capacity;
		this.hashmap = new HashMap<String, DataContainer>(capacity);
		this.queue = new DoubleLinkedList();
	}

	/**
	 * add value to the data structure. complexity is o(1)
	 * 
	 * @param key
	 * @param value
	 * @param timeToLive
	 */
	public void put(String key, Object value, int timeToLive) {
		synchronized (hashmap) {

			if (hashmap.size() == maxCapacity) {
				DataContainer dcRemoved = queue.removeHead();
				hashmap.remove(dcRemoved.getKey());
			}

			DataContainer dcExist = hashmap.get(key);

			if (dcExist != null) {
				queue.remove(dcExist);
			}

			DataContainer dcAdd = queue.add(key, value);
			hashmap.put(key, dcAdd);

			if (timeToLive > 0) {
				CompletableFuture.runAsync(() -> {
					try {
						Thread.sleep(timeToLive);
						remove(key, dcAdd);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				});
			}
		}
	}

	/**
	 * remove value from the data structure. complexity is o(1)
	 * 
	 * @param key
	 */
	public void remove(String key) {
		synchronized (hashmap) {
			DataContainer dcRemoved = hashmap.remove(key);
			queue.remove(dcRemoved);
		}
	}

	/**
	 * get value from the data structure. complexity is o(1)
	 * 
	 * @param key
	 * @return
	 */
	public Object get(String key) {
		synchronized (hashmap) {
			DataContainer dcGet = hashmap.get(key);
			if (dcGet != null) {
				return dcGet.getData();
			}
			return null;
		}
	}

	/**
	 * get number of keys in the data structure. complexity is o(1)
	 * 
	 * @return number of keys in the data structure
	 */
	public int size() {
		synchronized (hashmap) {
			return hashmap.size();
		}
	}

	/**
	 * remove value from the data structure. complexity is o(1) double checks that
	 * the mapped value was not updated
	 * 
	 * @param key
	 * @param dataContainer
	 */
	private void remove(String key, DataContainer dataContainer) {
		synchronized (hashmap) {
			if (hashmap.get(key) == dataContainer) {
				DataContainer dcRemoved = hashmap.remove(key);
				queue.remove(dcRemoved);
			}
		}
	}

}
